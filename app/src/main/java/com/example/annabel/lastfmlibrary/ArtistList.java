package com.example.annabel.lastfmlibrary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArtistList {

    @SerializedName("artists")
    @Expose
    private Artists artists;

    public Artists getArtists() {
        return artists;
    }

    public void setArtists(Artists artists) {
        this.artists = artists;
    }

    public class Artists {

        @SerializedName("artist")
        @Expose
        private List<Artist> artist = null;
        @SerializedName("@attr")
        @Expose
        private Attr attr;

        public List<Artist> getArtist() {
            return artist;
        }

        public void setArtist(List<Artist> artist) {
            this.artist = artist;
        }

        public Attr getAttr() {
            return attr;
        }

        public void setAttr(Attr attr) {
            this.attr = attr;
        }

    }
    public class Attr {

        @SerializedName("user")
        @Expose
        private String user;
        @SerializedName("page")
        @Expose
        private String page;
        @SerializedName("perPage")
        @Expose
        private String perPage;
        @SerializedName("totalPages")
        @Expose
        private String totalPages;
        @SerializedName("total")
        @Expose
        private String total;

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public String getPerPage() {
            return perPage;
        }

        public void setPerPage(String perPage) {
            this.perPage = perPage;
        }

        public String getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(String totalPages) {
            this.totalPages = totalPages;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

    }
}
