package com.example.annabel.lastfmlibrary;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private List<User> userList;
    final private UserClickListener userClickListener;

    public UserAdapter(List<User> userList, UserClickListener userClickListener) {
        this.userList = userList;
        this.userClickListener = userClickListener;
    }

    public interface UserClickListener{
        void userOnClick (int i);
    }

    @NonNull
    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_view, viewGroup, false);
        UserAdapter.ViewHolder viewHolder = new UserAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapter.ViewHolder viewHolder, int i) {
        User user =  userList.get(i);
        viewHolder.username.setText(user.getUsername());
        //viewHolder.description.setText(bucketItem.getBucketDescription());
    }

    @Override
    public int getItemCount() { return userList.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView username;

        public ViewHolder(View itemView) {
            super(itemView);
            username= itemView.findViewById(R.id.usernameView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            userClickListener.userOnClick(clickedPosition);
        }
    }

    public void updateList (List<User> newList) {
        userList = newList;
        if (newList != null) {
            this.notifyDataSetChanged();
        }
    }
}