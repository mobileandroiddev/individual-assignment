package com.example.annabel.lastfmlibrary;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    public LiveData<List<User>> getAllUsers();

    @Insert
    public void insertUser(User user);

    @Delete
    public void deleteUser(User user);

    @Update
    public void updateUser(User user);
}

