package com.example.annabel.lastfmlibrary;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class LibraryActivity extends AppCompatActivity {

    private List<Artist> artists;
    private ArtistAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);
        recyclerView = findViewById(R.id.artistRecycler);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        artists = MainActivity.getInstance().artists;
        updateUI();
    }

    private void updateUI() {
        if (adapter == null) {
            adapter = new ArtistAdapter(artists);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.updateList(artists);
        }
    }
}
