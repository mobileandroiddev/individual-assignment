package com.example.annabel.lastfmlibrary;

import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ViewHolder> {

    private List<Artist> artists;

    public ArtistAdapter(List<Artist> artists) {
        this.artists = artists;
    }


    @NonNull
    @Override
    public ArtistAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.artist_view, viewGroup, false);
        ArtistAdapter.ViewHolder viewHolder = new ArtistAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistAdapter.ViewHolder viewHolder, int i) {
        Artist artist =  artists.get(i);
        viewHolder.title.setText(artist.getName());
        if (!artist.getImage().get(3).getText().isEmpty()){
            Picasso.get().load(artist.getImage().get(3).getText()).into(viewHolder.artistImage);
        }
    }

    @Override
    public int getItemCount() { return artists.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView title;
//        public TextView playCount;
        public ImageView artistImage;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            artistImage = itemView.findViewById(R.id.imageView);
            //description = itemView.findViewById(R.id.descriptionView);
            //itemView.setOnClickListener(this);
        }

    }

    public void updateList (List<Artist> newList) {
        artists = newList;
        if (newList != null) {
            this.notifyDataSetChanged();
        }
    }
}
