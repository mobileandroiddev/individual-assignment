package com.example.annabel.lastfmlibrary;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class UserRepo{

    private AppDatabase appDatabase;
    private UserDao userDao;
    private LiveData<List<User>> users;
    private Executor executor = Executors.newSingleThreadExecutor();

    public UserRepo (Context context) {
        appDatabase = AppDatabase.getInstance(context);
        userDao = appDatabase.userDao();
        users = userDao.getAllUsers();
    }

    public LiveData<List<User>> getAllUsers() {
        return users;
    }

    public void insert(final User user) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                userDao.insertUser(user);
            }
        });
    }

    public void update(final User user) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                userDao.updateUser(user);
            }
        });
    }

    public void delete(final User user) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                userDao.deleteUser(user);
            }
        });
    }
}
