package com.example.annabel.lastfmlibrary;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class MainActivity extends AppCompatActivity implements UserAdapter.UserClickListener{

    private String username = "";
    private String method = "library.getartists";
    private String api_key = "8c9e8d3814a37c9f15fd5842cbcbb4fc";
    private String format = "json";
    private Integer artist_limit = 102;
    private Button addButton;
    private EditText userNameInput;
    private List<User> userList;
    private UserAdapter adapter;
    private RecyclerView recyclerView;
    private MainViewModel mainViewModel;
    private static MainActivity instance;
    public ArtistList artistList;
    public List<Artist> artists;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addButton = findViewById(R.id.button);
        userNameInput = findViewById(R.id.usernameInput);
        instance = this;

        recyclerView = findViewById(R.id.userRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        userList = new ArrayList<>();

        mainViewModel = new MainViewModel(getApplicationContext());
        mainViewModel.getUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {
                userList = users;
                updateUI();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = userNameInput.getText().toString();
                User user = new User(username);
                mainViewModel.insert(user);
                //requestData();
            }
        });

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            //Called when a user swipes left or right on a ViewHolder
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                //Get the index corresponding to the selected position
                int position = (viewHolder.getAdapterPosition());
                mainViewModel.delete(userList.get(position));
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

    }

    private void requestData(String name) {
        LastfmApiService service = LastfmApiService.retrofit.create(LastfmApiService.class);

        Call<ArtistList> call = service.getArtistLibrary(method, api_key, artist_limit, name, format);

        call.enqueue(new Callback<ArtistList>() {

            @Override
            public void onResponse(Call<ArtistList> call, Response<ArtistList> response) {
                artistList = response.body();
                artists = artistList.getArtists().getArtist();
                Intent intent = new Intent(MainActivity.this, LibraryActivity.class);
                startActivityForResult(intent, 1111);
            }

            @Override
            public void onFailure(Call<ArtistList> call, Throwable t) {
                Log.d("ERROR",t.toString());
            }
        });
    }

    public interface LastfmApiService {
        String BASE_URL = "http://ws.audioscrobbler.com/";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        @FormUrlEncoded
        @POST("2.0/")
        Call<ArtistList> getArtistLibrary(@Field("method") String method,
                                          @Field("api_key") String api_key,
                                          @Field("limit") Integer limit,
                                          @Field("user") String username,
                                          @Field("format") String format);

    }

    public static MainActivity getInstance(){
        return instance;
    }

    @Override
    public void userOnClick(int i) {
        requestData(userList.get(i).getUsername());
    }

    private void updateUI() {
        if (adapter == null) {
            adapter = new UserAdapter(userList, this);
            recyclerView.setAdapter(adapter);
        } else {
            adapter.updateList(userList);
        }
    }
}
